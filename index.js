const express = require('express')
const mongoose = require('mongoose')


const app = express()
const port = 3001



mongoose.connect(`mongodb+srv://admin123:admin123@zuitt-bootcamp.jzdol.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB'))

app.use(express.json()) 
app.use(express.urlencoded({extended: true}))


const userSchema = new mongoose.Schema ({
	username: String,
	password: String
})

const User = mongoose.model('User', userSchema)

app.post('/signup', (request, response) =>{

	User.findOne({username: request.body.username},(error, result) =>{
		
		if(result != null && result.username == request.body.username){
			return response.send('User already existed!')
		} else {
			if(request.body.username !== '' && request.body.password !== ''){
				let oneUser = new User({
					username: request.body.username,
					password: request.body.password
				})
				oneUser.save((error, doneUser) =>{
					if(error){
						return response.send(error)
					}
					return response.send('New user registered')
				})
			} else {
				return response.send('BOTH Username AND Password must be provided.')
			}
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))